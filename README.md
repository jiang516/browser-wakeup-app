#           

## 浏览器唤起软件

> ### linux
> 在软件对应的.desktop文件增加字段`MimeType=x-scheme-handler/testurlschemes`(testurlschemes必须纯英文的小写)
> 在`/usr/share/applications/`目录下创建*.desktop文件（如果是手动改这个文件，执行`update-desktop-database`立即生效）,文件内容如下
> ```
> [Desktop Entry]
> GenericName=web transmit adapter
> Name=web_transmit_adapter
> Version=1.0.0
> Exec=bash /usr/bin/web_transmit_adapter/web_transmit_adapter.sh
> Comment=web transmit adapter on Linux x64
> Icon=web_transmit_adapter.png
> Type=Application
> Terminal=false
> StartupNotify=false
> Encoding=UTF-8
> Categories=Utility
> MimeType=x-scheme-handler/testurlschemes
> ```
> ### windows
> 只要向注册表的HKEY_CLASSES_ROOT中写入指定格式的信息，即可
> ```
> Windows Registry Editor Version 5.00
> [HKEY_CLASSES_ROOT\TestUrlSchemes]
> @="Test URL Schemes"
> "URL Protocol"=""
> [HKEY_CLASSES_ROOT\TestUrlSchemes\shell\open\command]
> @="\"c:\\myapp.exe\" \"%1\""
> ```
