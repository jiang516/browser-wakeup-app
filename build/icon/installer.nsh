!macro customInstall
  WriteRegStr HKCR "videomeeting" "" "视频会议插件"
  WriteRegStr HKCR "videomeeting" "URL Protocol" ""
 # WriteRegStr HKCR "TestUrlSchemes\DefaultIcon" "" "$INSTDIR\视频会议插件.exe,0"
 # WriteRegStr HKCR "TestUrlSchemes\shell" "" ""
 # WriteRegStr HKCR "TestUrlSchemes\shell\open" "" ""
  WriteRegStr HKCR "videomeeting\shell\open\command" "" '"$INSTDIR\视频会议插件.exe" "%1"'
!macroend

!macro customUnInstall
     DeleteRegKey HKCR "videomeeting"
 !macroend
