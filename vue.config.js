module.exports = {
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        appId: 'd6281067-06ff-11ec-a201-005056ba5c9c',
        productName: '视频会议插件', // 项目名，也是生成的安装文件名，即aDemo.exe
        copyright: 'Copyright © 2021', // 版权信息
        directories: {
          output: './build' // 输出文件路径
        },
        electronDownload: {
          mirror: 'https://npm.taobao.org/mirrors/electron/'
        },
        // win相关配置
        win: {
          icon: './shanqis.ico', // 图标，当前图标在根目录下，注意这里有两个坑
          target: [
            {
              target: 'nsis', // 利用nsis制作安装程序
              arch: [
                'ia32' // 32位
              ]
            }
          ]
        },
        nsis: {
          oneClick: false, // 是否一键安装
          perMachine: true,
          allowElevation: true, // 允许请求提升。 如果为false，则用户必须使用提升的权限重新启动安装程序。
          allowToChangeInstallationDirectory: true, // 允许修改安装目录
          createDesktopShortcut: false, // 创建桌面图标
          createStartMenuShortcut: false, // 创建开始菜单图标
          shortcutName: '视频会议插件', // 图标名称
          include: 'build/icon/installer.nsh',
          guid: 'd6281067-06ff-11ec-a201-005056ba5c9c'
        },
        linux: {
          target: [
            {
              target: 'AppImage'
            }
          ],
          // "icon": "./public/app.ico",
          desktop: {
            GenericName: 'web transmit adapter',
            Name: '视频会议插件',
            Version: '1.0.0',
            // Exec: 'bash /usr/bin/web_transmit_adapter/web_transmit_adapter.sh',
            Comment: '视频会议插件',
            // Icon: './public/app.ico',
            Type: 'Application',
            Terminal: false,
            StartupNotify: false,
            Encoding: 'UTF-8',
            Categories: 'Utility',
            MimeType: 'x-scheme-handler/videomeeting'
          }
        }
      }
    }
  }
}
